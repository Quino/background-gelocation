package com.transistorsoft.cordova.bggeo;

import java.io.Serializable;

public class TreeNode implements Serializable {

	private static final long serialVersionUID = 1853260688492897809L;
	
	private Long id;
	private String name;
	private String description;
	private String notificationmsg;
	private Boolean notificationsrv;
	private Double ptalat;
	private Double ptalon;
	private Double ptblat;
	private Double ptblon;
	private Double extraptlat;
	private Double extraptlon;
	private Double extraradius;
	private Double extraheadingfrom;
	private Double extraheadingto;
	private TreeNode leftTree;
	private TreeNode rightTree;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotificationmsg() {
		return notificationmsg;
	}

	public void setNotificationmsg(String notificationmsg) {
		this.notificationmsg = notificationmsg;
	}

	public Boolean getNotificationsrv() {
		return notificationsrv;
	}

	public void setNotificationsrv(Boolean notificationsrv) {
		this.notificationsrv = notificationsrv;
	}

	public Double getPtalat() {
		return ptalat;
	}

	public void setPtalat(Double ptalat) {
		this.ptalat = ptalat;
	}

	public Double getPtalon() {
		return ptalon;
	}

	public void setPtalon(Double ptalon) {
		this.ptalon = ptalon;
	}

	public Double getPtblat() {
		return ptblat;
	}

	public void setPtblat(Double ptblat) {
		this.ptblat = ptblat;
	}

	public Double getPtblon() {
		return ptblon;
	}

	public void setPtblon(Double ptblon) {
		this.ptblon = ptblon;
	}

	public Double getExtraptlat() {
		return extraptlat;
	}

	public void setExtraptlat(Double extraptlat) {
		this.extraptlat = extraptlat;
	}

	public Double getExtraptlon() {
		return extraptlon;
	}

	public void setExtraptlon(Double extraptlon) {
		this.extraptlon = extraptlon;
	}

	public Double getExtraradius() {
		return extraradius;
	}

	public void setExtraradius(Double extraradius) {
		this.extraradius = extraradius;
	}

	public Double getExtraheadingfrom() {
		return extraheadingfrom;
	}

	public void setExtraheadingfrom(Double extraheadingfrom) {
		this.extraheadingfrom = extraheadingfrom;
	}

	public Double getExtraheadingto() {
		return extraheadingto;
	}

	public void setExtraheadingto(Double extraheadingto) {
		this.extraheadingto = extraheadingto;
	}

	public TreeNode getLeftTree() {
		return leftTree;
	}

	public void setLeftTree(TreeNode leftTree) {
		this.leftTree = leftTree;
	}

	public TreeNode getRightTree() {
		return rightTree;
	}

	public void setRightTree(TreeNode rightTree) {
		this.rightTree = rightTree;
	}

	@Override
	public String toString() {
		return "TreeNode [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

	
}
