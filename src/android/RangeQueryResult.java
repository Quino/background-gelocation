package com.transistorsoft.cordova.bggeo;

import java.util.ArrayList;
import java.util.Collection;

public class RangeQueryResult {

	private boolean areaA;
	private boolean areaB;
	private boolean areaC;
	private Collection<TreeNode> nearests;
	
	public RangeQueryResult() {
		areaA = false;
		areaB = false;
		areaC = false;
		nearests = new ArrayList<TreeNode>();
	}

	public boolean isAreaA() {
		return areaA;
	}

	public void setAreaA(boolean areaA) {
		this.areaA = areaA;
	}

	public boolean isAreaB() {
		return areaB;
	}

	public void setAreaB(boolean areaB) {
		this.areaB = areaB;
	}

	public boolean isAreaC() {
		return areaC;
	}

	public void setAreaC(boolean areaC) {
		this.areaC = areaC;
	}

	public Collection<TreeNode> getNearests() {
		return nearests;
	}

	public void setNearests(Collection<TreeNode> nearests) {
		this.nearests = nearests;
	}

	@Override
	public String toString() {
		boolean first = true;
		StringBuffer nearestBuffer = new StringBuffer();
		for(TreeNode node : nearests) {
			if (first) {
				first = false;
			}
			else {
				nearestBuffer.append(", ");
			}
			nearestBuffer.append(node.getName());
		}
		return "RangeQueryResult: areaA=" + areaA + ", areaB=" + areaB + ", areaC=" + areaC + ", nearests=[" + nearestBuffer + "]";
	}
}
